<?php
    function ubah_huruf($string){
        $new_string = [];
        for($i=0;$i<strlen($string);$i++){
            if($string[$i] == 'z'){
                $new_string[] = 'a';
            }
            else if($string[$i] == 'Z'){
                $new_string[] = 'A';
            }
            else{
                $new_string[] = chr(ord($string[$i])+1);
            }
        }
        return implode($new_string);

    }
?>