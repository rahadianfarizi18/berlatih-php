<?php
function tentukan_nilai($number)
{
    if($number >= 85 && $number <= 100){
        return "Sangat Baik";
    }
    if($number >= 70){
        return "Baik";
    }
    if($number >= 60){
        return "Cukup";
    }
    if($number >= 0){
        return "Kurang";
    }
}
?>